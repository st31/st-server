<?php

namespace app\common\services;

use app\common\enums\AccessEnum;
use app\common\enums\TimeEnum;
use app\modules\user\models\User;


class UserModelService
{
    /**
     * @param $model User
     *
     * @return User
     */
    public static function fillModelOfUserAdditionalInfo($model)
    {
        $model->created_at = time();
        $model->updated_at = time();

        return $model;
    }

    /**
     * @param $request array
     * @param $isAdmin bool
     *
     * @return array
     */
    public static function cleaningDataOfEditProfile($request, $isAdmin = false)
    {
        if (!$isAdmin && array_key_exists(AccessEnum::ID, $request)) {
            unset($request[AccessEnum::ID]);
        }

        if (array_key_exists(AccessEnum::LOGIN, $request)) {
            unset($request[AccessEnum::LOGIN]);
        }

        if (array_key_exists(AccessEnum::PASSWORD, $request)) {
            unset($request[AccessEnum::PASSWORD]);
        }

        if (array_key_exists(AccessEnum::USER_TYPE, $request)) {
            unset($request[AccessEnum::USER_TYPE]);
        }

        if (array_key_exists(TimeEnum::CREATED_AT, $request)) {
            unset($request[TimeEnum::CREATED_AT]);
        }

        if (array_key_exists(TimeEnum::UPDATED_AT, $request)) {
            unset($request[TimeEnum::UPDATED_AT]);
        }

        return $request;
    }
}