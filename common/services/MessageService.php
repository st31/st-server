<?php


namespace app\common\services;


/**
 * Class MessageService
 *
 * - Message code is serial number of message
 *
 *
 * @package app\common\services
 */
class MessageService
{
    /**
     * @return array
     */
    public static function messageErrorGenerateServiceInfo()
    {
        return MessageService::createMessage(1, 'Error generate service information');
    }

    /**
     * @param $login string
     *
     * @return array
     */
    public static function messageRegistrationSuccess($login)
    {
        $additionalInfo = [
            'is_registration' => TRUE,
            'login' => $login,
        ];

        return MessageService::createMessage(2, 'Registration success', $additionalInfo);
    }

    /**
     * @return array
     */
    public static function messageLoginOrPasswordNotCorrect()
    {
        return MessageService::createMessage(3, 'Login or password not correct');
    }

    /**
     * @return array
     */
    public static function messageAuthSuccess($UUID)
    {
        $additionalInfo = [
            'UUID' => $UUID
        ];

        return MessageService::createMessage(4, 'Auth success', $additionalInfo);
    }

    /**
     * @return array
     */
    public static function messageUUIDNotCorrect()
    {
        return MessageService::createMessage(5, 'Your UUID not correct');
    }

    /**
     * @return array
     */
    public static function messageUserNotFound()
    {
        return MessageService::createMessage(6, 'User not found');
    }

    /**
     * @return array
     */
    public static function messageProfileEditedSuccessFully()
    {
        return MessageService::createMessage(7, 'Profile edited successfully');
    }

    /**
     * @return array
     */
    public static function messageProfileEditedFailed()
    {
        return MessageService::createMessage(8, 'Profile edited failed');
    }

    /**
     * @return array
     */
    public static function messageNewPasswordNotCorrect()
    {
        return MessageService::createMessage(9, 'New password not correct');
    }

    /**
     * @return array
     */
    public static function messagePasswordEditSuccessfully()
    {
        return MessageService::createMessage(10, 'Password edited successfully');
    }

    /**
     * @return array
     */
    public static function messageLogoutFailed()
    {
        return MessageService::createMessage(11, 'Logout failed');
    }

    /**
     * @return array
     */
    public static function messageLogoutSuccessfully()
    {
        return MessageService::createMessage(12, 'Logout successfully');
    }


    /**
     * @param $code integer
     * @param $message string
     * @param $additionalInfo object
     *
     * @return array
     */
    private static function createMessage($code, $message, $additionalInfo = null)
    {
        $result = [
            'code' => $code,
            'message' => $message,
        ];

        if ($additionalInfo != null) {
            $result['additional_info'] = $additionalInfo;
        }

        return $result;
    }
}