<?php

namespace app\common\services;

use app\common\enums\AccessEnum;
use Yii;
use yii\web\Session;


class SessionService
{
    /**
     * @return Session
     */
    public static function openSession()
    {
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        return $session;
    }

    /**
     * @return int
     */
    public static function getUserId()
    {
        $userId = 0;
        $session = self::openSession();

        if ($session->has(AccessEnum::ID)) {
            $userId = $session->get(AccessEnum::ID);
        }

        $session->close();

        return $userId;
    }

    /**
     * @return void
     */
    public static function clearSession()
    {
        $session = self::openSession();

        if ($session->count() == 0) {
            return;
        }

        $session->removeAll();
        $session->close();
    }
}