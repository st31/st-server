<?php

namespace app\common\services;

use app\common\enums\AccessEnum;
use app\common\enums\MessageEnum;
use app\modules\user\models\User;
use Yii;
use Faker\Provider\Uuid;
use yii\web\Session;


/**
 * Class AccessService
 *
 * @package app\common\services
 */
class AccessService
{
    /**
     * @return bool
     */
    public static function checkAccess()
    {
        return true;
    }

    /**
     * @return string
     */
    public static function generateUUID()
    {
        return Uuid::uuid();
    }

    /**
     * @param $UUID string
     * @param $userId integer
     *
     * @return bool
     */
    public static function saveAuth($UUID, $userId)
    {
        if (is_null($UUID) || $userId < 0) {
            return false;
        }

        /** @var User $model */
        $model = User::find()->where('id = :id', [':id' => $userId])->one();

        if (is_null($model)) {
            return false;
        }

        $model->updated_at = time();
        if (!$model->save()) {
            return false;
        }

        $session = SessionService::openSession();
        $session->set(AccessEnum::UUID, $UUID);
        $session->set(AccessEnum::ID, $userId);

        $session->close();

        return true;
    }

    /**
     * @param $password string
     * @param $passwordHash string
     *
     * @return bool
     */
    public static function validatePassword($password, $passwordHash)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $passwordHash);
    }

    /**
     * @param $UUID string
     *
     * @return bool
     */
    public static function checkAuth($UUID)
    {
        $session = SessionService::openSession();
        $isAuth = false;

        if (!is_null($UUID) && $session->has(AccessEnum::UUID) && self::checkCorrectUUID($UUID)) {
            if ($UUID == $session->get(AccessEnum::UUID)) {
                $isAuth = true;
            }
        }

        $session->close();

        return $isAuth;
    }

    /**
     * @param $UUID string
     *
     * @return bool
     */
    public static function checkCorrectUUID($UUID)
    {
        return strlen($UUID) == 36;
    }

    /**
     * @param $userId integer
     *
     * @return array
     */
    public static function getUser($userId)
    {
        $isAdmin = AccessService::checkRoleAdmin();
        $request = UserModelService::cleaningDataOfEditProfile(Yii::$app->request->post(), $isAdmin);

        $result = [
            'type' => 'error',
            'value' => null
        ];

        if (!array_key_exists(AccessEnum::UUID, $request)) {
            $result['value'] = [MessageEnum::ERROR => MessageService::messageUUIDNotCorrect()];

            return $result;
        }

        $UUID = $request[AccessEnum::UUID];
        if (is_null($UUID) || !AccessService::checkAuth($UUID)) {
            $result['value'] = [MessageEnum::ERROR => MessageService::messageUUIDNotCorrect()];

            return $result;
        }

        $userId = $isAdmin ? $request[AccessEnum::ID] : $userId;
        $model = User::find()->where('id = :id', [':id' => $userId])->one();
        if (is_null($model)) {
            $result['value'] = [MessageEnum::ERROR => MessageService::messageUserNotFound()];

            return $result;
        }

        $result['value'] = $model;
        $result['type'] = 'user';

        return $result;
    }

    /**
     * @return bool
     */
    public static function checkRoleAdmin()
    {
        $session = SessionService::openSession();

        $isAdmin = false;
        if ($session->has(AccessEnum::ID) && $session->get(AccessEnum::ID) == 1) {
            $isAdmin = true;
        }

        $session->close();

        return $isAdmin;
    }
}