<?php


namespace app\common\enums;


class AccessEnum
{
    const UUID = 'UUID';
    const LOGIN = 'login';
    const PASSWORD = 'password';
    const ID = 'id';
    const USER_TYPE = 'user_type';
}