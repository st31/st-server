<?php


namespace app\common\enums;


class TimeEnum
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}