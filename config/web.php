<?php

use yii\web\ForbiddenHttpException;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'on beforeAction' => function ($event) {

        // this is code...
        // в event есть данные о текущем урле
        // через глобальные массивы смотрим

        // die;
    },
    'components' => [
        'response' => [
            'class' => 'yii\web\Response',
            'format' => 'json',

            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null) {
                    $data = $response->data;
                    $error = '';
                    if (!$response->isSuccessful) {
                        if (isset($data['message'])) {
                            $error = $data['message'];
                        } elseif (isset(current($data)['message'])) {
                            $error = current($data)['message'];
                        }
                    }
                    $response->data = [
                        'status' => $response->statusCode,
                        'code' => $response->statusCode,
                        'error' => $error,
                    ];
                    if ($response->isSuccessful) {
                        $response->data['data'] = $data;
                    }

                    $response->data['timestamp'] = [
                        'date_time' => Yii::$app->formatter->asDatetime(time(), Yii::$app->formatter->dateFormat),
                        'time_zone' => Yii::$app->timeZone
                    ];
                } else {
                    $forbiddenException = new ForbiddenHttpException();
                    $response->data = [
                        'status' => $response->isSuccessful,
                        'code' => $forbiddenException->statusCode,
                        'error' => 'Insufficient rights',
                    ];

                    $response->data['timestamp'] = [
                        'date_time' => Yii::$app->formatter->asDatetime(time(), Yii::$app->formatter->dateFormat),
                        'time_zone' => Yii::$app->timeZone
                    ];
                }
            },
        ],

        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUS',
            'locale' => 'ru-RU',
        ],
        'request' => [
            'cookieValidationKey' => 'cjrJZR5ip1S44SaeIksUPYAefs-F2Mwx',
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => TRUE,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => TRUE,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => TRUE,
            'enableStrictParsing' => TRUE,
            'showScriptName' => FALSE,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller' => ['user/control'],
                    'prefix' => 'api',
                    'extraPatterns' => [
                        'GET /' => 'default',
                        'POST /' => 'default',

                        'POST registration' => 'registration',
                        'OPTIONS registration' => 'registration',

                        'POST auth' => 'auth',
                        'OPTIONS auth' => 'auth',

                        'POST edit' => 'edit',
                        'OPTIONS edit' => 'edit',

                        'GET profile' => 'profile',
                        'OPTIONS profile' => 'profile',

                        'POST edit-password' => 'edit-password',
                        'OPTIONS edit-password' => 'editPassword',

                        'GET logout' => 'logout',
                        'OPTIONS logout' => 'logout',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'basePath' => '@app/modules/user',
            'class' => 'app\modules\user\Module',
        ],
    ],

    'params' => $params,
];

//if (YII_ENV_DEV) {
//    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//        // uncomment the following to add your IP if you are not connecting from localhost.
//        //'allowedIPs' => ['127.0.0.1', '::1'],
//    ];
//
//    $config['bootstrap'][] = 'gii';
//    $config['modules']['gii'] = [
//        'class' => 'yii\gii\Module',
//        // uncomment the following to add your IP if you are not connecting from localhost.
//        //'allowedIPs' => ['127.0.0.1', '::1'],
//    ];
//}

return $config;
