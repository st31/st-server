<?php

use yii\db\Migration;

/**
 * Class m190831_120422_remove_column_uuid_from_user_table
 */
class m190831_120422_remove_column_uuid_from_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'UUID');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user', 'UUID', 'varchar(36) AFTER `user_type`');
    }
}
