<?php

use yii\db\Migration;

/**
 * Class m190831_002736_update_column_password_into_user_table
 */
class m190831_002736_update_column_password_into_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'password', 'varchar(100)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user', 'password', 'varchar(32)');
    }
}
