<?php

use yii\db\Migration;

/**
 * Class m190831_101613_add_column_uuid_into_user_table
 */
class m190831_101613_add_column_uuid_into_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'UUID', 'varchar(36) AFTER `user_type`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'UUID');
    }
}
