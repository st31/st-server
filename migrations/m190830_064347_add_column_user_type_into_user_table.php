<?php

use yii\db\Migration;

/**
 * Class m190830_064347_add_column_user_type_into_user_table
 */
class m190830_064347_add_column_user_type_into_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'user_type', "ENUM('student','teacher') AFTER `email`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'user_type');
    }
}
