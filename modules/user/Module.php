<?php

namespace app\modules\user;

use app\common\services\AccessService;
use yii\base\Action;


/**
 * user module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\user\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * @param $action Action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        return AccessService::checkAccess();
    }
}
