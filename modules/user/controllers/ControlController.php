<?php

namespace app\modules\user\controllers;

use app\common\enums\AccessEnum;
use app\common\enums\MessageEnum;
use app\common\services\AccessService;
use app\common\services\MessageService;
use app\common\services\SessionService;
use app\common\services\UserModelService;
use app\modules\user\models\User;
use Yii;
use yii\base\Exception;
use yii\rest\ActiveController;
use yii\web\Response;


class ControlController extends ActiveController
{
    public $modelClass = 'app\modules\user\models\User';


    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['index', 'view', 'create', 'update', 'search'],
                'formats' => ['application/json' => Response::FORMAT_JSON],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionDefault()
    {
        return json_encode(['error' => 'Method default']);
    }

    /**
     * @return array
     */
    public function actionRegistration()
    {
        $result = null;
        $model = new User();
        $model = UserModelService::fillModelOfUserAdditionalInfo($model);

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            try {
                $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            } catch (Exception $e) {
                return [MessageEnum::ERROR => MessageService::messageErrorGenerateServiceInfo()];
            }

            if ($model->save()) {
                $result = MessageService::messageRegistrationSuccess($model->login);
            } else {
                $result = [MessageEnum::ERROR => $model->errors];
            }
        } else {
            $result = [MessageEnum::ERROR => $model->errors];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function actionAuth()
    {
        $login = Yii::$app->request->post()[AccessEnum::LOGIN];
        $password = Yii::$app->request->post()[AccessEnum::PASSWORD];

        if (is_null($login) || is_null($password)) {
            return [MessageEnum::ERROR => MessageService::messageLoginOrPasswordNotCorrect()];
        }

        /** @var User $model */
        $model = User::find()->where('login = :login', [':login' => $login])->one();

        if (is_null($model)) {
            return [MessageEnum::ERROR => MessageService::messageLoginOrPasswordNotCorrect()];
        }

        $UUID = AccessService::generateUUID();
        if (AccessService::validatePassword($password, $model->password) &&
            AccessService::saveAuth($UUID, $model->id)
        ) {
            return MessageService::messageAuthSuccess($UUID);
        } else {
            return [MessageEnum::ERROR => MessageService::messageLoginOrPasswordNotCorrect()];
        }
    }

    /**
     * @return array
     */
    public function actionEdit()
    {
        $isAdmin = AccessService::checkRoleAdmin();
        $request = UserModelService::cleaningDataOfEditProfile(Yii::$app->request->post(), $isAdmin);
        $userId = $isAdmin ? $request[AccessEnum::ID] : SessionService::getUserId();

        $user = AccessService::getUser($userId);
        if ($user['type'] == 'error') {
            return $user['value'];
        }

        /** @var User $model */
        $model = $user['value'];

        if ($model->load($request, '') && $model->validate()) {
            $model->updated_at = time();

            if ($model->save()) {
                return MessageService::messageProfileEditedSuccessFully();
            }
        }

        return [MessageEnum::ERROR => MessageService::messageProfileEditedFailed()];
    }

    /**
     * @return array | User
     */
    public function actionProfile()
    {
        $isAdmin = AccessService::checkRoleAdmin();
        $request = UserModelService::cleaningDataOfEditProfile(Yii::$app->request->post(), $isAdmin);
        $userId = $isAdmin ? $request[AccessEnum::ID] : SessionService::getUserId();

        $user = AccessService::getUser($userId);
        if ($user['type'] == 'error') {
            return $user['value'];
        }

        /** @var User $model */
        $model = $user['value'];

        if (is_null($model)) {
            return [MessageEnum::ERROR => MessageService::messageUserNotFound()];
        }

        unset($model->password);

        return $model;
    }

    /**
     * @return array
     */
    public function actionEditPassword()
    {
        $request = Yii::$app->request->post();
        if (!array_key_exists(AccessEnum::PASSWORD, $request)) {
            return [MessageEnum::ERROR => MessageService::messageNewPasswordNotCorrect()];
        }

        $isAdmin = AccessService::checkRoleAdmin();
        $userId = $isAdmin ? $request[AccessEnum::ID] : SessionService::getUserId();

        $user = AccessService::getUser($userId);
        if ($user['type'] == MessageEnum::ERROR) {
            return $user['value'];
        }

        /** @var User $model */
        $model = $user['value'];

        if (is_null($model)) {
            return [MessageEnum::ERROR => MessageService::messageUserNotFound()];
        }

        $newPasswordHash = null;
        try {
            $newPasswordHash = Yii::$app->getSecurity()->generatePasswordHash($request[AccessEnum::PASSWORD]);
        } catch (Exception $e) {
            return [MessageEnum::ERROR => $e->getMessage()];
        }

        $model->password = $newPasswordHash;
        $model->updated_at = time();
        if ($model->validate() && $model->save()) {
            return MessageService::messagePasswordEditSuccessfully();
        } else {
            return [MessageEnum::ERROR => $model->errors];
        }
    }

    /**
     * @return array
     */
    public function actionLogout()
    {
        $userId = SessionService::getUserId();

        if ($userId == 0) {
            return [MessageEnum::ERROR => MessageService::messageLogoutFailed()];
        }

        SessionService::clearSession();

        return MessageService::messageLogoutSuccessfully();
    }
}
