<?php

namespace app\modules\user\models;

use yii\db\ActiveRecord;


/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $email
 * @property string $user_type
 * @property int $created_at
 * @property int $updated_at
 */
class User extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password', 'email'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['login', 'name', 'surname', 'patronymic', 'email'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 100],
            [['user_type'], 'string'],
            [['login'], 'unique'],
            [['email'], 'unique'],
            [
                ['login', 'email', 'user_type', 'name', 'surname', 'patronymic'],
                'filter', 'filter' => 'trim', 'skipOnArray' => true
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'name' => 'Name',
            'surname' => 'Surname',
            'patronymic' => 'Patronymic',
            'email' => 'Email',
            'user_type' => 'User Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


}
